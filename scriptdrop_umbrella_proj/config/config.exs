# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of the Config module.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

# Sample configuration:
#
#     config :logger, :console,
#       level: :info,
#       format: "$date $time [$level] $metadata$message\n",
#       metadata: [:user_id]
#

config :phxapp, PhxappWeb.Endpoint,
  url: [host: "localhost"],
  http: [port: 4000],
  secret_key_base: "NSyrvvwpaAsH569JGjrvFO3dHXO4zoeaH5Jng749n9PwUmD0vUMHTxt8s4SINKMw",
  render_errors: [view: PhxappWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Phxapp.PubSub, adapter: Phoenix.PubSub.PG2]

config :phoenix, :json_library, Jason

config :phxapp, authentication: [
    username: "user",
    password: "password",
    realm: "Dev Realm"
]
